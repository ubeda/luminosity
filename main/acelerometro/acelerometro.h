/*
 * lis3dh_example.h
 *
 *  Created on: 9 mar. 2021
 *      Author: dismuntel
 */

#ifndef MAIN_LIS3DH_EXAMPLE_H_
#define MAIN_LIS3DH_EXAMPLE_H_

#include <stdint.h>
#include <stdbool.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_event_loop.h"
#include "esp_event.h"
#define LOG_LOCAL_LEVEL ESP_LOG_INFO
#include "esp_log.h"
#include "sntp.h"
#include "math.h"

#include "lis3dh.h"

extern float aceleracionX;
extern float aceleracionY;
extern float aceleracionZ;

extern bool measureAccOk;
extern bool superaLimite;
extern bool vibracion;
extern int UmbralModulo;

extern float module;
extern bool sensor_acelerometro;

extern lis3dh_float_data_t  data;
extern short pitch;
extern short roll;

void read_data ();
void user_task_interrupt (void *pvParameters);
void IRAM int_signal_handler (uint8_t gpio);
void user_task_periodic(void *pvParameters);
void acelerometro_init(void);



#endif /* MAIN_LIS3DH_EXAMPLE_H_ */
