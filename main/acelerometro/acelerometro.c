#include <stdio.h>
#include "acelerometro.h"

#include "esp_event.h"
#include "esp_timer.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/ledc.h"
#include "esp_err.h"
#include "sdkconfig.h"
#define LOG_LOCAL_LEVEL ESP_LOG_INFO
#include "esp_log.h"

/**
 * Simple example with one sensor connected to I2C or SPI. It demonstrates the
 * different approaches to fetch the data. Either one of the interrupt signals
 * is used or new data are fetched periodically.
 *
 * Hardware configuration:
 *
 *   I2C
 *
 *   +-----------------+   +----------+
 *   | ESP8266 / ESP32 |   | LIS3DH   |
 *   |                 |   |          |
 *   |   GPIO 14 (SCL) ----> SCL      |
 *   |   GPIO 13 (SDA) <---> SDA      |
 *   |   GPIO 5        <---- INT1     |
 *   +-----------------+   +----------+
 *
 *   SPI
 *
 *   +-----------------+   +----------+      +-----------------+   +----------+
 *   | ESP8266         |   | LIS3DH   |      | ESP32           |   | LIS3DH   |
 *   |                 |   |          |      |                 |   |          |
 *   |   GPIO 14 (SCK) ----> SCK      |      |   GPIO 16 (SCK) ----> SCK      |
 *   |   GPIO 13 (MOSI)----> SDI      |      |   GPIO 17 (MOSI)----> SDI      |
 *   |   GPIO 12 (MISO)<---- SDO      |      |   GPIO 18 (MISO)<---- SDO      |
 *   |   GPIO 2  (CS)  ----> CS       |      |   GPIO 19 (CS)  ----> CS       |
 *   |   GPIO 5        <---- INT1     |      |   GPIO 5        <---- INT1     |
 *   +-----------------+    +---------+      +-----------------+   +----------+
 */

/* -- use following constants to define the example mode ----------- */

// #define SPI_USED     // SPI interface is used, otherwise I2C
 //#define FIFO_MODE    // multiple sample read mode
 //#define INT_DATA     // data interrupts used (data ready and FIFO status)
 //#define INT_EVENT    // inertial event interrupts used (wake-up, free fall or 6D/4D orientation)
 //#define INT_CLICK    // click detection interrupts used

#if defined(INT_DATA) || defined(INT_EVENT) || defined(INT_CLICK)
#define INT_USED
#endif

/* -- includes ----------------------------------------------------- */

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_event_loop.h"
#include "esp_event.h"
#define LOG_LOCAL_LEVEL ESP_LOG_INFO
#include "esp_log.h"
#include "sntp.h"
#include "math.h"

#include "lis3dh.h"

/** -- platform dependent definitions ------------------------------ */

#ifdef ESP_PLATFORM  // ESP32 (ESP-IDF)

// user task stack depth for ESP32
#define TASK_STACK_DEPTH 2048

// SPI interface definitions for ESP32
#define SPI_BUS       HSPI_HOST
#define SPI_SCK_GPIO  16
#define SPI_MOSI_GPIO 17
#define SPI_MISO_GPIO 18
#define SPI_CS_GPIO   19


#else  // ESP8266 (esp-open-rtos)

// user task stack depth for ESP8266
#define TASK_STACK_DEPTH 256

// SPI interface definitions for ESP8266
#define SPI_BUS       1
#define SPI_SCK_GPIO  14
#define SPI_MOSI_GPIO 13
#define SPI_MISO_GPIO 12
#define SPI_CS_GPIO   2   // GPIO 15, the default CS of SPI bus 1, can't be used

#endif  // ESP_PLATFORM

// I2C interface defintions for ESP32 and ESP8266
//SDO low!
#define I2C_BUS       0
#define I2C_SCL_PIN   14
#define I2C_SDA_PIN   13
#define I2C_FREQ      I2C_FREQ_100K

// interrupt GPIOs defintions for ESP8266 and ESP32
#define INT1_PIN      5
#define INT2_PIN      4


//static const char TAG[] = "Acelerometro";

/* -- user tasks --------------------------------------------------- */

static lis3dh_sensor_t* sensor;

/**
 * Common function used to get sensor data.
 */

lis3dh_float_data_t  data;
float g=9.81;
float module;
bool sensor_acelerometro = false; 		//Si no envia datos (fallos en conexion) se pone a false;
bool vibracion = false; 				//Indica si hay vibracion, es decir, las aceleraciones han superado el l�mite establecido
int UmbralModulo = 500;   //500 = 0.5 g
bool measureAccOk = false;
int counterAcc = 0;

short pitch;
short roll;

void read_data ()
{
    #ifdef FIFO_MODE

    lis3dh_float_data_fifo_t fifo;

    if (lis3dh_new_data (sensor))	//Compruebo si hay nueva data
    {
        uint8_t num = lis3dh_get_float_data_fifo (sensor, fifo);	//Obtengo el número de nueva data que hay

        printf("%.3f LIS3DH num=%d\n", (double)sdk_system_get_time()*1e-3, num); //Printeo el número de nueva data que hay

        for (int i=0; i < num; i++)		//Printeo las aceleraciones de x,y,z para cada nueva data que hay
            // max. full scale is +-16 g and best resolution is 1 mg, i.e. 5 digits
            printf("%.3f LIS3DH (xyz)[g] ax=%+7.3f ay=%+7.3f az=%+7.3f\n",
                   (double)sdk_system_get_time()*1e-3,
                   fifo[i].ax, fifo[i].ay, fifo[i].az);

    } else printf("lis3dh_new_data ha fallado \n");

    #else

    counterAcc++;											//Mientras no obtengamos nueva medida, aumentamos el contador
    if(counterAcc > 15000) sensor_acelerometro = false;		//Si el contador es mayor que 15000, damos el acelerometro por inhabilitado

    if (lis3dh_new_data (sensor) && lis3dh_get_float_data (sensor, &data)){
    	measureAccOk = true;			//Indica que se ha obtenido correctamente el valor del acelerometro
    	sensor_acelerometro = true;		//Sensor funciona correctamente
    	counterAcc = 0; 				//Cuando obtenemos nuevo valor del acelerometro, reseteamos contador

        module = 1000 * ( sqrt( pow(data.ax,2) + pow(data.ay,2) + pow(data.az,2) ));	//Modulo de un vector de 3 dimensiones. Unidades del m�dulo: g, EJ: module = 3 significa |a| = 3 * g = 3 * 9.81 m^2/s

        pitch  = (short)(atan2((float)(0-data.ay),data.az) * 180 / 3.14159);     //Convert to degrees
        roll  = (short)(atan2((float)(data.ax),data.az) * 180 / 3.14159);        //Convert to degrees

        //ESP_LOGI(TAG,"x: %f , y: %f , z: %f",data.ax,data.ay,data.az);


        if (abs(module) >= UmbralModulo) {		//Module esta en g's. El umbral son 5 g's, pero como multiplicamos por 10 el module para que sea un numero significativo, el umbral seran 50
        	//ESP_LOGW("MAIN","Hemos superado el umbral de %d /1000 g con un modulo de %.3f /1000 g",UmbralModulo,module);
        	vibracion = true;
        }  else vibracion = false;

        //Aunque s�lo mostramos el m�dulo cuando hemos superado el l�mite, se est� calculando continuamente para poder enviarlo por CAN
         //if (vibracion)  ESP_LOGW(TAG, "Vibracion | Aceleraciones [g]: X = %f , Y = %f , Z = %f , modulo = %f \n", data.ax, data.ay, data.az, module);

	}
    #endif // FIFO_MODE
}


#ifdef INT_USED
/**
 * In this case, any of the possible interrupts on interrupt signal *INT1* is
 * used to fetch the data.
 *
 * When interrupts are used, the user has to define interrupt handlers that
 * either fetches the data directly or triggers a task which is waiting to
 * fetch the data. In this example, the interrupt handler sends an event to
 * a waiting task to trigger the data gathering.
 */

static QueueHandle_t gpio_evt_queue = NULL;

// User task that fetches the sensor values.

void user_task_interrupt (void *pvParameters)
{
    uint8_t gpio_num;

    while (1)
    {
        if (xQueueReceive(gpio_evt_queue, &gpio_num, portMAX_DELAY))
        {
            lis3dh_int_data_source_t  data_src  = {};
            lis3dh_int_event_source_t event_src = {};
            lis3dh_int_click_source_t click_src = {};

            // get the source of the interrupt and reset *INTx* signals
            //Resetea las interrupciones y obtiene la fuente de las interrupciones por data, evento y click
            #ifdef INT_DATA
            lis3dh_get_int_data_source  (sensor, &data_src);
            #endif
            #ifdef INT_EVENT
            lis3dh_get_int_event_source (sensor, &event_src, lis3dh_int_event1_gen);
            #endif
            #ifdef INT_CLICK
            lis3dh_get_int_click_source (sensor, &click_src);
            #endif

            // in case of DRDY interrupt or inertial event interrupt read one data sample
            //Si hay información lista para leer --> read_data()
            if (data_src.data_ready){
            	printf("Data ready \n");
                read_data ();
            }


            // in case of FIFO interrupts read the whole FIFO
            //Cuando haya una interrupción de evento o el FIFO esté lleno --> read_data()
            else  if (data_src.fifo_watermark || data_src.fifo_overrun){
                if(data_src.fifo_watermark) printf("Water mark surpassed \n");
                if(data_src.fifo_overrun) printf("Fifo buffer is full \n");
            	read_data ();
            }
            // in case of event interrupt
            //Si hay alguna coordenada por debajo o por arriba de su threshold --> printf indicando cuál
            else if (event_src.active)
            {
                printf("%.3f LIS3DH \n", (double)sdk_system_get_time()*1e-3);
                if (event_src.x_low)  printf("x is lower than threshold\n");
                if (event_src.y_low)  printf("y is lower than threshold\n");
                if (event_src.z_low)  printf("z is lower than threshold\n");
                if (event_src.x_high) printf("x is higher than threshold\n");
                if (event_src.y_high) printf("y is higher than threshold\n");
                if (event_src.z_high) printf("z is higher than threshold\n");
            }

            // in case of click detection interrupt
            else if (click_src.active)
               printf("%.3f LIS3DH %s CLICK ACTIVO\n", (double)sdk_system_get_time()*1e-3,
                      click_src.s_click ? "single click" : "double click");
        } else printf("No recibimos nada \n");
    }
}

// Interrupt handler which resumes user_task_interrupt on interrupt

void IRAM int_signal_handler (uint8_t gpio)
{
    // send an event with GPIO to the interrupt user task
    xQueueSendFromISR(gpio_evt_queue, &gpio, NULL);
}

#else // !INT_USED

/*
 * In this example, user task fetches the sensor values every seconds.
 */

void user_task_periodic(void *pvParameters)
{
    vTaskDelay (100/portTICK_PERIOD_MS);

    while (1)
    {
        read_data();
    }
}

#endif // INT_USED

/* -- main program ------------------------------------------------- */

void acelerometro_init(void)
{
    // Set UART Parameter.
    uart_set_baud(0, 115200);
    // Give the UART some time to settle
    vTaskDelay(1);

    /** -- MANDATORY PART -- */

    #ifdef SPI_USED

    // init the sensor connnected to SPI
    spi_bus_init (SPI_BUS, SPI_SCK_GPIO, SPI_MISO_GPIO, SPI_MOSI_GPIO);

    // init the sensor connected to SPI_BUS with SPI_CS_GPIO as chip select.
    sensor = lis3dh_init_sensor (SPI_BUS, 0, SPI_CS_GPIO);

    #else

    // init all I2C bus interfaces at which LIS3DH  sensors are connected
    i2c_init (I2C_BUS, I2C_SCL_PIN, I2C_SDA_PIN, I2C_FREQ);

    // init the sensor with slave address LIS3DH_I2C_ADDRESS_1 connected to I2C_BUS.
    sensor = lis3dh_init_sensor (I2C_BUS, LIS3DH_I2C_ADDRESS_1, 0);

    #endif

    if (sensor)
    {
        #ifdef INT_USED

        /** --- INTERRUPT CONFIGURATION PART ---- */

        // Interrupt configuration has to be done before the sensor is set
        // into measurement mode to avoid losing interrupts

        // create an event queue to send interrupt events from interrupt
        // handler to the interrupt task
        gpio_evt_queue = xQueueCreate(10, sizeof(uint8_t));

        // configure interupt pins for *INT1* and *INT2* signals and set the interrupt handler
        gpio_enable(INT1_PIN, GPIO_INPUT);
        gpio_set_interrupt(INT1_PIN, GPIO_INTTYPE_EDGE_POS, int_signal_handler);

        #endif  // INT_USED

        /** -- SENSOR CONFIGURATION PART --- */

        // set polarity of INT signals if necessary
         //lis3dh_config_int_signals (sensor, lis3dh_high_active);

        #ifdef INT_DATA
        // enable data interrupts on INT1 (data ready or FIFO status interrupts)
        // data ready and FIFO status interrupts must not be enabled at the same time
        #ifdef FIFO_MODE
        lis3dh_enable_int (sensor, lis3dh_int_fifo_overrun  , lis3dh_int1_signal, true);
        lis3dh_enable_int (sensor, lis3dh_int_fifo_watermark, lis3dh_int1_signal, true);
        //lis3dh_enable_int (sensor, lis3dh_int_fifo_overrun, lis3dh_int1_signal, true);
        #else
        lis3dh_enable_int (sensor, lis3dh_int_data_ready, lis3dh_int1_signal, true);
        #endif // FIFO_MODE
        #endif // INT_DATA

        #ifdef INT_EVENT
        // enable data interrupts on INT1
        lis3dh_int_event_config_t event_config;

        event_config.mode = lis3dh_wake_up;
        // event_config.mode = lis3dh_free_fall;
        // event_config.mode = lis3dh_6d_movement;
        // event_config.mode = lis3dh_6d_position;
        // event_config.mode = lis3dh_4d_movement;
        // event_config.mode = lis3dh_4d_position;
        event_config.threshold = 10;
        event_config.x_low_enabled  = false;
        event_config.x_high_enabled = true;
        event_config.y_low_enabled  = false;
        event_config.y_high_enabled = true;
        event_config.z_low_enabled  = false;
        event_config.z_high_enabled = true;
        event_config.duration = 0;
        event_config.latch = true;

        lis3dh_set_int_event_config (sensor, &event_config, lis3dh_int_event1_gen);
        lis3dh_enable_int (sensor, lis3dh_int_event1, lis3dh_int1_signal, true);
        #endif // INT_EVENT

        #ifdef INT_CLICK
        // enable click interrupt on INT1
        lis3dh_int_click_config_t click_config;

        click_config.threshold = 10;
        click_config.x_single = false;
        click_config.x_double = false;
        click_config.y_single = false;
        click_config.y_double = false;
        click_config.z_single = true;
        click_config.z_double = false;
        click_config.latch = true;
        click_config.time_limit   = 1;
        click_config.time_latency = 1;
        click_config.time_window  = 3;

        lis3dh_set_int_click_config (sensor, &click_config);
        lis3dh_enable_int (sensor, lis3dh_int_click, lis3dh_int1_signal, true);
        #endif // INT_CLICK

        #ifdef FIFO_MODE
        // clear FIFO and activate FIFO mode if needed
        lis3dh_set_fifo_mode (sensor, lis3dh_bypass,  0, lis3dh_int1_signal);	//Limpia configuración FIFO
        lis3dh_set_fifo_mode (sensor, lis3dh_stream, 10, lis3dh_int1_signal);	//Configura modo FIFO
        #endif

        /*
         * NOTA: Se presentan picos aleatorios en las medidas. Para filtrarlos, se ha aumentado el valor de la frecuencia de corte (tercer campo en "lis3dh_config_hpf")
         */
        //Por defecto: //lis3dh_hpf_normal
        // configure HPF and reset the reference by dummy read
  //      lis3dh_config_hpf (sensor, lis3dh_hpf_normal_x, 200, true, true, true, true);
  //      lis3dh_get_hpf_ref (sensor);

        // enable ADC inputs and temperature sensor for ADC input 3
        //lis3dh_enable_adc (sensor, true, true);

        // LAST STEP: Finally set scale and mode to start measurements
        //configuración inicial del sensor
        lis3dh_set_scale(sensor, lis3dh_scale_2_g);
        lis3dh_set_mode (sensor, lis3dh_odr_25, lis3dh_normal, true, true, true); //lis3dh_odr_10 . 10 Hz implica 10 medidas por segundo

        /** -- TASK CREATION PART --- */

        // must be done last to avoid concurrency situations with the sensor
        // configuration part

        #ifdef INT_USED

        // create a task that is triggered only in case of interrupts to fetch the data
        xTaskCreate(user_task_interrupt, "user_task_interrupt", TASK_STACK_DEPTH, NULL, 2, NULL);

        #else // !INT_USED

        // create a user task that fetches data from sensor periodically
        xTaskCreate(user_task_periodic, "Tarea Acelerometro", TASK_STACK_DEPTH, NULL, 12, NULL);

        #endif
    }
    else
        printf("Could not initialize LIS3DH sensor\n");
}

