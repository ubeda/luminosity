/*
 * bme680_user.h
 *
 *  Created on: 16 nov. 2021
 *      Author: PC
 */

#ifndef MAIN_BME680_BME680_USER_H_
#define MAIN_BME680_BME680_USER_H_

extern bme680_values_float_t bme680;

void bme680_test(void *pvParameters);
void init_bme680();



#endif /* MAIN_BME680_BME680_USER_H_ */
