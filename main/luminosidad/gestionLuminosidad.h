/*
 * gestionLuminosidad.h
 *
 *  Created on: 26 dic. 2021
 *      Author: Mateo Ubeda
 */

#ifndef MAIN_LUMINOSIDAD_GESTIONLUMINOSIDAD_H_
#define MAIN_LUMINOSIDAD_GESTIONLUMINOSIDAD_H_

#define n_registers 3			//Numero de registros
#define size_register 5		//Size de cada registro
uint8_t current_register;		//Indica el registro actual en el que estamos trabajando

typedef struct {

	struct tm tiempo[size_register];
	float luminosidad[size_register];
	bool saved;
	struct tm date_saved [2];

} __attribute__ ((packed)) sistema;

void init_store_lux_data(void);
void timer_store_data_cb(void* arg);
void register_full (void);
void print_register(uint8_t Register, bool all);

#endif /* MAIN_LUMINOSIDAD_GESTIONLUMINOSIDAD_H_ */
