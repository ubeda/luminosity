/*
 * sensorLuminosidad.h
 *
 *  Created on: 23 mar. 2021
 *      Author: mateo
 */

#ifndef COMPONENTS_LUMINOSIDAD_SENSORLUMINOSIDAD_H_
#define COMPONENTS_LUMINOSIDAD_SENSORLUMINOSIDAD_H_

#include <stdio.h>
#include <stdlib.h>

#include "driver/i2c.h"

extern float sensorLux;
extern bool sensor_luminosidad;

//Comandos
#define ALS_CONFIG 		0x00	//Escritura. Enviar configuraci�n del sensor
#define ALS_PS 			0x03	//Escritura. Enviar parametros de Power Saving
#define ALS_OUTPUT 		0x04	//Lectura. Leer los valores del sensor

//Ganancias
#define GAIN_1 	 		0b00
#define GAIN_2 	 		0b01
#define GAIN_1_8 		0b10
#define GAIN_1_4 		0b11

//Tiempo de interrupcion en milisegundos
#define IT_25_H 		0b11
#define IT_25_L 		0b00
#define IT_50_H 		0b10
#define IT_50_L 		0b00
#define IT_100_H 		0b00
#define IT_100_L 		0b00
#define IT_200_H 		0b00
#define IT_200_L 		0b01
#define IT_400_H 		0b00
#define IT_400_L 		0b10
#define IT_800_H 		0b00
#define IT_800_L 		0b11

//Persistence protect number setting
#define PERS_1 	 		0b00
#define PERS_2 	 		0b01
#define PERS_4 			0b10
#define PERS_8 			0b11

//Interrupciones
#define INT_DIS 	 	0b0		//Disable
#define INT_EN		 	0b1		//Enable

//"Apagar" sensor
#define SD_DIS 	 		0b0		//Power on
#define SD_EN 	 		0b1		//Shut down

//Power Saving Mode				//Cuanto el n�mero del modo sea m�s grande, m�s r�pido es el tiempo de refresco del sensor y menos ahorro energ�tico
#define MODE_1			0b00
#define MODE_2			0b01
#define MODE_3			0b10
#define MODE_4			0b11

//Enable Power Saving
#define DISABLE_PS		0b0
#define ENABLE_PS		0b1

//Bits reservados vac�os
#define RESERVED_15_13 	0b000
#define RESERVED_10 	0b0
#define RESERVED_3_2 	0b00
#define RESERVED_8		0b00000000
#define RESERVED_5		0b00000


esp_err_t i2c_master_read_slave(i2c_port_t i2c_num, uint8_t *data_rd, size_t size);
esp_err_t i2c_master_write_slave(i2c_port_t i2c_num, uint8_t *data_wr, size_t size);
esp_err_t i2c_master_sensor_test(i2c_port_t i2c_num, uint8_t *data_h, uint8_t *data_l);
void init_sensor_luminosidad();
esp_err_t i2c_slave_init();
void disp_buf(uint8_t *buf, int len);
void i2c_test_task(void *arg);



#endif /* COMPONENTS_LUMINOSIDAD_SENSORLUMINOSIDAD_H_ */
