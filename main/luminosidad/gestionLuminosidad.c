/*
 * gestionLuminosidad.c
 *
 *  Created on: 26 dic. 2021
 *      Author: Mateo Ubeda
 */

#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include <stdio.h>
#include "time.h"
#include <time.h>
#include "string.h"
#include "lwip/err.h"
#include "lwip/sys.h"
#include <stdlib.h>
#include <errno.h>
#include "math.h"

#include <stdio.h>
#include <string.h>
#include <sys/unistd.h>
#include <sys/stat.h>
#include "esp_err.h"
#include "esp_log.h"

#include "conf.h"
#include "nvs.h"
#include "led.h"
#include "sensorLuminosidad.h"

#include "gestionLuminosidad.h"

extern sistema datos[n_registers];				//Estructura principal donde almaceno todos los datos utiles
extern struct tm time_screen;					//Hora epoch con zona horaria

static const char *TAG = "Lux";
timer_declaration(timer_store_data);	//Declaracion del timer de recogida de datos

/*
 * void timer_store_data_cb(void* arg);
 * Callback del timer declarado en el init.
 * Cada vez que salta, comprueba si la hora es la correcta y si el sensor de luminosidad esta funcionando
 * Si esta todo ok, se guarda la hora y el valor de lux en la estructura datos
 * Cuando vea que todo un registro esta lleno, llama a register_full() para que lo guarde en la memoria flash
 *
 */
void timer_store_data_cb(void* arg) {		//Timer callback que muestra y guarda periodicamente toda la informacion en el registro determinado

	char buffer[64];
	rtc_time_get_now_with_tz(&time_screen);	//Obtenemos hora
	if(sensor_luminosidad && (time_screen.tm_year>100)) {		//Si funciona el sensor y la hora esta correcta
		if(!datos[current_register].saved){						//Si hay algun registro que no este ya guardado (y por lo tanto, lleno):
			for(int j3=0;j3<size_register;j3++){			    //Paso por todos los campos posibles dentro de un mismo registro
				if(datos[current_register].luminosidad[j3] < 0) {   //Si encontramos un hueco vacio (negativo significa vacio), guardamos el dato
					memcpy(&datos[current_register].tiempo[j3],&time_screen,sizeof(struct tm));  //Guardamos la hora
					datos[current_register].luminosidad[j3] = sensorLux;						   //Guardamos valor de luminosidad
					strftime(buffer, 64, "%d/%m/%Y %H:%M:%S", &datos[current_register].tiempo[j3]);

					ESP_LOGI(TAG,"Time: %s - Lux sensor:%.2f ",
							buffer, datos[current_register].luminosidad[j3]);

					if(j3 == (size_register-1)) { 	//Si llegamos al ultimo campo, notificamos que ya tenemos un registro lleno
						register_full();				//Notificamos que tenemos un registro lleno
					}
					break;						    //Salimos del bucle cuando hayamos guardado el valor
				}
			}
		}
	}
}

/*
 * void register_full ();
 * Cuando un registro esta lleno, se tiene que llamar a esta funcion
 * Guarda toda la informacion util en la memoria flash y la muestra por pantalla
 * Comprueba si estan todos los registros llenos. Si es asi, para el timer
 * Si no es asi, pone por defecto el siguiente registro para que se empiece a rellenar
 *
 */

void register_full (){	//Cuando se notifica un registro lleno, lo guarda en la memoria flash y prepara el siguiente registro

	esp_err_t err;

	ESP_LOGW(TAG,"Registro %d esta lleno",current_register);

	datos[current_register].saved = true;	//Indico que el registro actual ya esta lleno y guardado

	//Guardo hora inicial del registro
	memcpy(&datos[current_register].date_saved[0],&datos[current_register].tiempo[0],sizeof(struct tm));

	//Guardo hora final del registro
	memcpy(&datos[current_register].date_saved[1],&datos[current_register].tiempo[(size_register-1)],sizeof(struct tm));

	err = writeNVS("misparametros",&datos,n_registers*sizeof(sistema));		//Guardo en la memoria flash el registro lleno

	if(err != ESP_OK) ESP_LOGE(TAG,"writeNVS not successful - (%s) ",esp_err_to_name(err));

	print_register(current_register,false);					//Muestro por pantalla todos los valores del registro actual
	if(n_registers<=3) set_duty_led(255,current_register);	//Enciende LED si el registro esta lleno. < 3 ya que solo tenemos 3 LEDs

	//Si esta el ultimo registro posible esta guardado significa que ya estan todos guardados, por lo que paramos el timer
	if(datos[(n_registers-1)].saved) 	{
		ESP_LOGW(TAG,"Todos los registros estan llenos, paramos de recoger datos");
		timer_stop(timer_store_data);						//Declaracion del timer
	} else {	//Si aun queda algun registro por guardar:
		current_register++;		//Pasamos al siguiente registro
		for(int k=0;k<size_register;k++) 	datos[current_register].luminosidad[k] = -1;	//Ponemos valores por defecto
	}

}

/*
 * void print_register(uint8_t Register, bool all);
 * Muestra todo. el contenido del registro Register al completo
 * Tambien puede mostrar todos los registros de datos[] si all esta a true
 *
 */
void print_register(uint8_t Register, bool all){	//Si all esta a 1, muestro todos los registros. Si all esta a 0, solo muestro Register
	char buffer[64];
	memset(&buffer, 0, sizeof (buffer));

	for(int i1=0;i1<n_registers;i1++){
		if(datos[i1].saved){
			printf("---------------- \n");
			printf("    HISTORICO \n");
			printf("---------------- \n");

			if(all) printf("\n Registro numero: %d \n",i1);
			else printf("\n Registro numero: %d \n",Register);

			printf(" Size del historico: %d \n",size_register);

			if(all) strftime(buffer, 64, "%d/%m/%Y %H:%M:%S", &datos[i1].tiempo[0]);
			else strftime(buffer, 64, "%d/%m/%Y %H:%M:%S", &datos[Register].tiempo[0]);
			printf(" Hora de inicio: %s \n",buffer);

			if(all) strftime(buffer, 64, "%d/%m/%Y %H:%M:%S", &datos[i1].tiempo[(size_register-1)]);
			else strftime(buffer, 64, "%d/%m/%Y %H:%M:%S", &datos[Register].tiempo[(size_register-1)]);
			printf(" Hora final: %s \n",buffer);

			printf("\n HORA \n");
			for(int j1=0;j1<size_register;j1++) {
				if(all) strftime(buffer, 64, "%H:%M ", &datos[i1].tiempo[j1]);
				else strftime(buffer, 64, "%H:%M ", &datos[Register].tiempo[j1]);
				printf(" %s ",buffer);
			}

			printf("\n\n LUMINOSIDAD AMBIENTAL\n");
			for(int k1=0;k1<size_register;k1++) {
				if(all) printf(" %.2f",datos[i1].luminosidad[k1]);
				else printf(" %.2f",datos[Register].luminosidad[k1]);
			}

			printf("\n");

			if(!all) break;	//Si no queremos mostrar todos los registros, salimos cuando hayamos mostrado el deseado
		}
	}
}

/*
 * void init_store_lux_data();
 * Comprueba si existe algun registro guardado en la memoria flash. Si es asi muestra todo el registro
 * El primer registro que se encuentre vacio, lo pone por defecto para que se empiece a rellenar
 * Crea timer, su callback y especifica periodo de tiempo en el que salta el callback de guardado de datos
 *
 */
void init_store_lux_data(){
	ESP_LOGI(TAG,"Size del paquete guardado %d",(int) n_registers*sizeof(sistema));
	current_register = 0;
	for(int i=0;i<n_registers;i++){							//Pasamos por todos los registros
		if(datos[i].saved) {								//Si hay algun registro que ya este guardado y terminado:
			ESP_LOGW(TAG,"Tenemos registro guardado, lo mostramos:");
			print_register(i,false);						//Mostramos todo el registro
			if(n_registers<=3) set_duty_led(255,i);	//Enciende LED si el registro esta lleno. < 3 ya que solo tenemos 3 LEDs
		} else {											//Si el registro no esta guardado:
			ESP_LOGW(TAG,"Ponemos por defecto el registro %d",i);
			current_register = i;							//Asignamos el registro con el que trabajaremos
			memset(&datos[i],0,sizeof(sistema));			//Lo vaciamos porque se va a usar a continuacion
			for(int j=0;j<size_register;j++) 	datos[i].luminosidad[j] = -1;	//Ponemos valores por defecto
			break;											//Salimos porque si no pasaria al siguiente registro vacio
		}
	}

		timer_creation(timer_store_data, timer_store_data_cb);		//Creacion del timer
		timer_start_periodic(timer_store_data, 500);				//Iniciamos el timer periodicamente con un periodo determinado
}
