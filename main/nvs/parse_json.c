/*
 * genericCom.c
 *
 *  Created on: 11 nov. 2020
 *      Author: rafa400
 */

#include "parse_json.h"

#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"
#include <freertos/timers.h>
#include "esp_system.h"

#ifndef CONFIG_LOG_DEFAULT_LEVEL
#define CONFIG_LOG_DEFAULT_LEVEL ESP_LOG_INFO
#endif
#ifndef LOG_LOCAL_LEVEL
#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE
#endif
#include "esp_log.h"
#include "esp_app_trace.h"
#include "esp_sleep.h"
#include "sdkconfig.h"

#include "parse_json.h"
#include "nvs.h"


static const char* TAG = "JSONConfigSmarWater";

char* g_nombre=NULL;
char* g_ssid=NULL;
char* g_password=NULL;

int size_nombre=0;
int size_ssid=0;
int size_password=0;


bool enmarcha=false;
char comandoSensor[50];

char* getStringMyJSON(cJSON *root,char* jsonfield) {
	cJSON *comando = cJSON_GetObjectItem(root,jsonfield);
	if (comando==NULL) {
		// ESP_LOGE(jsonconfigTAG,"campo %s no encontrado",jsonfield);
		return NULL;
	} else if (comando->type!=cJSON_String) {
		ESP_LOGE(TAG,"campo %s no es tipo 'string'",jsonfield);
		return NULL;
	} else{
		//printf("valuestring: %s \n",comando->valuestring);
		return comando->valuestring;
	}
}
double getNumberMyJSON(cJSON *root,char* jsonfield) {
	cJSON *comando = cJSON_GetObjectItem(root,jsonfield);						//Busca el valor correspondiente al campo "jsonfield" en la string de json
	if (comando==NULL) {														//Si el Json de entrada es vacio, devuelve -1000
		// ESP_LOGE(jsonconfigTAG,"campo %s no encontrado",jsonfield);
		return -1000;
	} else if (comando->type!=cJSON_Number) {									//Si el tipo no es num�rico, imprime error y devuelve -1000
		ESP_LOGE(TAG,"campo %s no es tipo 'number'",jsonfield);
		return -1000;
	} else
		return comando->valuedouble;		//Devuelve el valor obtenido correspondiente al campo "jsonfield"
}
bool getBoolMyJSON(cJSON *root,char* jsonfield) {
	cJSON *comando = cJSON_GetObjectItem(root,jsonfield);
	if (comando==NULL) {
		// ESP_LOGE(jsonconfigTAG,"campo %s no encontrado",jsonfield);
		return false;
	} else if (!cJSON_IsBool(comando)) {
		ESP_LOGE(TAG,"campo %s no es tipo 'bool'",jsonfield);
		return false;
	} else
		return true; // cJSON_IsTrue(comando);
}

cJSON* checkJSON(char* entrada) {  // recordar destruit el objeto root si har return != NULL
	cJSON *root;
	root = cJSON_Parse(entrada);
	if (root==NULL) {
		ESP_LOGE(TAG,"JSON de entrada mal formado");
		ESP_LOGE(TAG,"%s",entrada);
		cJSON_Delete(root);
		return NULL;
	}
	return root;
}

void iniciaJSON(char* nombre, int size_n, char* ssid, int size_s, char* psswd, int size_p) {
	g_nombre=nombre;
	g_ssid=ssid;
	g_password=psswd;
	size_nombre=size_n;
	size_ssid=size_s;
	size_password=size_p;

}

double getNumberSecondJSON(cJSON *root,char* jsonfield,char* jsonfield2) {		//Obtiene el valor num�rico de un subcampo
	 cJSON *comando = cJSON_GetObjectItem(root,jsonfield);						//Busca el valor correspondiente al campo "jsonfield" en la string de json
	 //return comando;

	 cJSON *comando2 = cJSON_GetObjectItem(comando,jsonfield2);						//Busca el valor correspondiente al campo "jsonfield" en la string de json
		if (comando2==NULL) {														//Si el Json de entrada es vacio, devuelve -1000
			//ESP_LOGE("GENERIC_GEST","campo %s no encontrado",jsonfield2);
			return -1000;
		} else if (comando2->type!=cJSON_Number) {									//Si el tipo no es num�rico, imprime error y devuelve -1000
			ESP_LOGE(TAG,"campo %s no es tipo 'number'",jsonfield2);
			return -1000;
		} else
			return comando2->valuedouble;

}

char* getStringSecondJSON(cJSON *root,char* jsonfield,char* jsonfield2) {
	cJSON *comando = cJSON_GetObjectItem(root,jsonfield);

	cJSON *comando2 = cJSON_GetObjectItem(comando,jsonfield2);
	if (comando2==NULL) {
		// ESP_LOGE(jsonconfigTAG,"campo %s no encontrado",jsonfield);
		return NULL;
	} else if (comando2->type!=cJSON_String) {
		ESP_LOGE(TAG,"campo %s no es tipo 'string'",jsonfield2);
		return NULL;
	} else{
		//printf("valuestring: %s \n",comando2->valuestring);
		return comando2->valuestring;
	}
}

bool entradaJSON(char* entrada) { // Devuelve true si han cambiado parámetros de iniciaJSON y necesita salvar
	bool haycambio=false;
	cJSON *root=checkJSON(entrada);
	if (root==NULL) return false;// sino, al acabar hay que hacer cJSON_Delete(root);

    if (getBoolMyJSON(root,"LSLEEP")) esp_light_sleep_start(); // {"LSLEEP":true }
    if (getBoolMyJSON(root,"DSLEEP")) esp_deep_sleep_start();  // { "DSLEEP":true}
    if (getBoolMyJSON(root,"REBOOT")) esp_restart(); // {"REBOOT":true } // Provoca reinicio del dispositivo
    if (getBoolMyJSON(root,"DEFAULT")) if (onSetToDefault!=NULL) onSetToDefault(NULL,NULL,0); //setParamToDefault();   // {"DEFAULT":true }
    char* name=getStringMyJSON(root,"NAME");
    if (name!=NULL) {
    	strncpy(g_nombre,name,size_nombre); // Payload: { "NAME":"casa" }  // Cambia el nombre del dispositivo
    	haycambio=true;
    }
   	char* ssid=getStringMyJSON(root,"SSID");  //  { "SSID":"XXX", "PSSWD":"XXX"}  // Cambia el SSID/PSSWD configurados
   	char* psswd=getStringMyJSON(root,"PSSWD");
   	if ((ssid!=NULL) && (psswd!=NULL)) {
   		strncpy(g_ssid,ssid,size_ssid);
   		strncpy(g_password,psswd,size_password);
   		haycambio=true;
   	}
    if (getBoolMyJSON(root,"UPDATE")) lanzarActualizacionOTA(entrada);  // { "UPDATE":true}

	cJSON_Delete(root);
	//if (haycambio) writeNVS("DISMUNTEL", &paramgenerales, sizeof(paramgenerales_t));
	return haycambio;
}

