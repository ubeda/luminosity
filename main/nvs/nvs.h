/*
 * NVFdismuntel.h
 *
 *  Created on: 26 nov. 2020
 *      Author: dismuntel
 */

#ifndef COMPONENTS_GENERICGEST_NVFDISMUNTEL_H_
#define COMPONENTS_GENERICGEST_NVFDISMUNTEL_H_

#include "esp_err.h"

#define STORAGE_NAMESPACE "dismuntel"

extern int (*onSetToDefault)(char* dataname, void* data, int datalenght);

void initNVS(int (*functionPtrSetParamToDefault)(char* dataname, void* data, int datalenght));
void* readNVS(char* dataname,void *data, int datalenght);
esp_err_t writeNVS(char* dataname, void* data, int datalenght);

esp_err_t lanzarActualizacionOTA(char *conf) ;

#endif /* COMPONENTS_GENERICGEST_NVFDISMUNTEL_H_ */




