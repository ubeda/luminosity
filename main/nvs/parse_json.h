/*
 * genericCom.h
 *
 *  Created on: 11 nov. 2020
 *      Author: rafa400
 */

#ifndef GENERICGEST_H_
#define GENERICGEST_H_

#include "stdbool.h"
#include "cJSON.h"
#include "nvs.h"

void iniciaJSON(char* nombre, int size_n, char* ssid, int size_s, char* psswd, int size_p);
double getNumberSecondJSON(cJSON *root,char* jsonfield,char* jsonfield2);
char* getStringSecondJSON(cJSON *root,char* jsonfield,char* jsonfield2);
bool entradaJSON(char* entrada);  // {"command":2, "LimAlPers":141, "LimAlRoom":101}
cJSON* checkJSON(char* entrada);  // Verificar si es o no un JSON y devolver el objeto parseado

char* getStringMyJSON(cJSON *root,char* jsonfield);
double getNumberMyJSON(cJSON *root,char* jsonfield);
bool getBoolMyJSON(cJSON *root,char* jsonfield);



#endif /* COMPONENTS_GENERICGEST_GENERICGEST_H_ */


