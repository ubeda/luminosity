/*
 * led_tricolor.h
 *
 *  Created on: 22 dic. 2020
 *      Author: dismuntel
 */

#ifndef LED_TRICOLOR_H_
#define LED_TRICOLOR_H_

//Las tres GPIOs se definen en el menuconfig

void initLedTricolor();
uint32_t getDutyLED(void);
void set_duty_led(uint32_t duty, int mode);
void set_duty_led_with_fade(uint32_t duty, int fade_time, uint8_t mode);
void enciende_1 (void);
void enciende_2 (void);
void enciende_3 (void);
void apaga_todo(void);

#endif /* MAIN_BUZZER_H_ */
