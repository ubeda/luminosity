/* servo motor control example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_attr.h"
#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE
#include "esp_log.h"
#include "esp_err.h"
#include "math.h"

#include "driver/mcpwm.h"
#include "soc/mcpwm_periph.h"

#include "servo_motor.h"

//You can get these value from the datasheet of servo you use, in general pulse width varies between 1000 to 2000 mocrosecond
#define SERVO_MIN_PULSEWIDTH 1000 //Minimum pulse width in microsecond	1000
#define SERVO_MAX_PULSEWIDTH 2000 //Maximum pulse width in microsecond	2000
#define SERVO_MAX_DEGREE 180 //Maximum angle in degree upto which servo can rotate

int32_t pulse_width;
esp_err_t err;
bool auto_activated = false;

/**
 * @brief Use this function to calcute pulse width for per degree rotation
 *
 * @param  degree_of_rotation the angle in degree to which servo has to rotate
 *
 * @return
 *     - calculated pulse width
 */
static int32_t degree_to_pulsewidth(int32_t degree_of_rotation)
{
    int32_t cal_pulsewidth = 0;
    cal_pulsewidth = (SERVO_MIN_PULSEWIDTH + (((SERVO_MAX_PULSEWIDTH - SERVO_MIN_PULSEWIDTH) * (degree_of_rotation)) / (SERVO_MAX_DEGREE)));
    return cal_pulsewidth;
}

/**
 * @brief Configure MCPWM module
 */
void servo_control_init(void)
{
    //1. mcpwm gpio initialization
    mcpwm_gpio_init(MCPWM_UNIT_0, MCPWM0A, 18);    //Set GPIO 18 as PWM0A, to which servo is connected

    //2. initial mcpwm configuration
    ESP_LOGI("Servo-motor","Configuring Initial Parameters of mcpwm");
    mcpwm_config_t pwm_config;
    pwm_config.frequency = 50;    //frequency = 50Hz, i.e. for every servo motor time period should be 20ms
    pwm_config.cmpr_a = 0;    //duty cycle of PWMxA = 0
    pwm_config.cmpr_b = 0;    //duty cycle of PWMxb = 0
    pwm_config.counter_mode = MCPWM_UP_COUNTER;
    pwm_config.duty_mode = MCPWM_DUTY_MODE_0;
    err = mcpwm_init(MCPWM_UNIT_0, MCPWM_TIMER_0, &pwm_config);    //Configure PWM0A & PWM0B with above settings  
	if(err!=0) ESP_LOGE("Servo-motor","Can't Configure Initial Parameters of mcpwm 0");
	
}

esp_err_t set_angle_servo_motor(int32_t angle){
	if(angle<=300){
		printf("Angle of rotation: %d\n", angle);
		pulse_width = degree_to_pulsewidth(angle);
		err = mcpwm_set_duty_in_us(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_A, pulse_width);
		//printf("duty cycle set: %d \n ", (int) pulse_width);
		vTaskDelay(10);     //Add delay, since it takes time for servo to rotate, generally 100ms/60degree rotation at 5V
	}
	return err;
}

void set_angle_servo_motor_365(void *arg){

	while(1){
		if(auto_activated){		//Si el modo auto esta activado
			for(int angle=0;angle<300;angle=angle+5){	//Giramos en un sentido de 0 a 300 grados
				if(auto_activated){		//Si desactivamos el modo auto en mitad de un bucle, gracias a este if, se interrumpiria el bucle
					pulse_width = degree_to_pulsewidth(angle);
					err = mcpwm_set_duty_in_us(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_A, pulse_width);
					vTaskDelay(10);
				} else break;
			}

			for(int angle=300;angle>0;angle=angle-5){	//Giramos en el otro sentido de 300 a 0 grados
					if(auto_activated){	    //Si desactivamos el modo auto en mitad de un bucle, gracias a este if, se interrumpiria el bucle
					pulse_width = degree_to_pulsewidth(angle);
					err = mcpwm_set_duty_in_us(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_A, pulse_width);
					vTaskDelay(10);
				} else break;
			}
		} else	vTaskDelay(300);

	}
}

float get_angle_servo_motor(void){
	float duty = 0;
	float angle = 0;
	duty = mcpwm_get_duty(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_A);	//Devuelve 2.5 para 0 grados y 12.5 para 300 grados
	angle = round((18.08*duty) - 36.035);
	printf("Duty Cycle get: %.2f - Angle get: %.2f \n",duty,angle);
	return angle;
}


