#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include <stdio.h>
#include "time.h"
#include <time.h>
#include "string.h"
#include "lwip/err.h"
#include "lwip/sys.h"
#include <stdlib.h>
#include <errno.h>
#include "math.h"

#include <stdio.h>
#include <string.h>
#include <sys/unistd.h>
#include <sys/stat.h>
#include "esp_err.h"
#include "esp_log.h"


#include "main.h"
#include "sensorLuminosidad.h"		//Sensor de luminosidad
#include "gestionLuminosidad.h"
#include "bme680_user.c"			//Sensor de temperatura y presion BME680
#include "conf.h"					//Configuracion (timer, reloj)
#include "wifi_station.h"			//Configuracion wifi station (conexion a AP)
#include "mqtt.h"					//Configuracion MQTT
#include "nvs.h"					//Guardar informacion en memoria flash
#include "parse_json.h"				//Parsear mensajes JSON
#include "led.h"					//Control de LEDs mediante PWM
#include "servo_motor.c"			//Servo motor
#include "acelerometro.h"			//Acelerometro

#define Luminosidad					//Activar funcionamiento del sensor de luminosidad
//#define Store_Data				//Activa modo recogida y almacenamiento de datos del sensor de luminosidad

static const char *TAG = "Node";
struct tm time_screen;					//Hora epoch con zona horaria
sistema datos[n_registers];				//Estructura principal donde almaceno todos los datos utiles
double angulo;
esp_err_t err_motor;
TaskHandle_t motor = NULL;
/*
 * Pantalla esta suscrito al topic STATUS
 * Nodo esta suscrito al topic COMANDO
 *
 * LUMINOSIDAD
 * Pantalla envia topic: "station/7C87CEE34580/COMANDO" con mensaje: {"cmd":"STATUS"}
 * Nodo responde topic: "station/C4DD57B81438/LUX" con mensaje: "{"cmd":"LUX","value":0.01}"
 * Pantalla recibe topic: "station/7C87CEE34580/LUX" con mensaje: "{"cmd":"LUX","value":0.01}"
 *
 * MOTOR
 * Pantalla envia topic: "station/7C87CEE34580/COMANDO" con mensaje: {"cmd":"MOTOR","set":25}
 * Nodo responde topic: "station/C4DD57B81438/STATUS" con mensaje: "{"cmd":"MOTOR","set":0.01,"error":"NO"}"
 * Pantalla recibe topic: "station/7C87CEE34580/STATUS" con mensaje: "{"cmd":"MOTOR","set":0.01,"error":"NO"}"
 */

int callback_MQTT(char *data){   //Cada vez que recibamos un mensaje MQTT del exterior (mqtt explorer o web), saltar� esta funci�n.

	cJSON *root;

	root = checkJSON(data);			//Recogemos el JSON que hemos recibido en data
	if (root==NULL) ESP_LOGE("MAIN","MQTT: Root not created");

	char* cmd=NULL;
	char* command = malloc(100*sizeof(char));
	char* message = malloc(100*sizeof(char));

	memset(command,0,100*sizeof(char));
	memset(message,0,100*sizeof(char));

    if ((cmd=getStringMyJSON(root,"cmd"))!=NULL) {	// {"cmd":"<comando>"}, donde <comando> puede ser INFO, STATUS, ORDER, etc
		   if (strcmp(cmd,"LUX")==0){	//Si "cmd" es "LUX"
			   ESP_LOGI(TAG,"Recibimos comando LUX");

			   if(sensor_luminosidad) snprintf(message,100,"{\"cmd\":\"LUX\",\"value\":%.2f}",sensorLux);
			   else snprintf(message,100,"{\"cmd\":\"LUX\",\"value\":%.2f}",0.01);
			   sendMQTT("station/C4DD57B81438/LUX",message);
		   }

		   if (strcmp(cmd,"GYRO")==0){	//Si "cmd" es "GYRO"
			   ESP_LOGI(TAG,"Recibimos comando GYRO");

			   if(sensor_luminosidad) snprintf(message,100,"{\"cmd\":\"GYRO\",\"pitch\":%d,\"roll\":%d}",pitch,roll);
			   else snprintf(message,100,"{\"cmd\":\"GYRO\",\"pitch\":%d,\"roll\":%d}",pitch,roll);
			   sendMQTT("station/C4DD57B81438/GYRO",message);
		   }

		   if (strcmp(cmd,"MOTOR")==0){	//Si "cmd" es "MOTOR"
			   ESP_LOGI(TAG,"Recibimos comando motor");
			   angulo = getNumberMyJSON(root,"set");				//Nos guardamos el campo recibido de "set"
			   if(angulo==365) {	//Si recibimos 365, nos estan diciendo que activemos modo automatico o que lo desactivemos
				   //Si la tarea modo automatico no esta creada, creala
				   if(motor==NULL) xTaskCreate(set_angle_servo_motor_365, "Motor automatico", 1024 * 4, (void *)0, 10, &motor);
				   if(!auto_activated){		//Si tenemos el modo auto desactivado, lo activamos
					   ESP_LOGW(TAG,"Activamos modo automatico");
					   auto_activated = 1;		//Modo auto activado
					   return 0;		//Si nos piden modo automatico, salimos y no calculamos nada m�s
				   } else {					//Si tenemos el modo auto activado, lo desactivamos
					   ESP_LOGW(TAG,"Desactivamos modo automatico");
					   auto_activated = 0;	//Desactivamos flag de modo automatico
					   //En este caso no hay return. Leemos el angulo real del motor y lo enviamos a la estacion
				   }
			   }
			   err_motor = set_angle_servo_motor(angulo);			//Enviamos angulo recibo al motor
			   float current_angle = get_angle_servo_motor();		//Leemos el angulo del motor despues de haberlo seteado
			   if(!err_motor) snprintf(message,100,"{\"cmd\":\"MOTOR\",\"set\":%.0f,\"get\":%.2f,\"error\":%d}",angulo,current_angle,0); //No error
			   else snprintf(message,100,"{\"cmd\":\"MOTOR\",\"set\":%.0f,\"get\":%.2f,\"error\":%d}",angulo,current_angle,1); //Si error
			   sendMQTT("station/C4DD57B81438/MOTOR",message);
		   }
    }

	cJSON_Delete(root);			//Borramos el paquete que hemos recibido

	free(message);
	free(command);

    return 0;
}



int setParamToDefault(char* dataname, void* data, int datalenght){

	if (strcmp("misparametros",dataname)==0) {
		sistema* param[n_registers];								//Creo una estructura para que se almacene en la flash y reserve ya su memoria

		writeNVS("misparametros",param,n_registers*sizeof(sistema));	//Escribo en la flash la configuracion por defecto
		ESP_LOGW("Param","Parametros Fabrica guardados!");
	}

	return 0;
}

void dynamic_motor(short roll_s){

	//printf("in \n");

	if(roll_s > 7){	//Si el acelerometro esta inclinado hacia la derecha
		set_angle_servo_motor(65);
	} else if (roll_s < -7) {  //Si el acelerometro esta inclinado hacia la izquierda
		set_angle_servo_motor(-65);
	} else {
		set_angle_servo_motor(0);
	}

}



void app_main(void)
{

	//Inicia particion NVS y define "setParamToDefault" como la funcion que se llamar� cuando no haya nada en la particion.
	//En los pr�ximos arranques, inicia la particion NVS y ejecuta el readNVS de despu�s correctamente porque la
    //particion ha sido escrita en el primer arranque
	//initNVS(setParamToDefault);

	//Con el primer arranque, readNVS fallar� porque la particion NVS estar� vac�a y se llamar� a setParamToDefault para cargar
	//y escribir en la particion NVS los valores por defecto
	//Con los siguientes arranques, se volcar� en pkt lo que est� guardado en la particion NVS
    //readNVS("misparametros",&datos, n_registers*sizeof(sistema));	//Lee los valores de la flash y los vuelca en la estructura datos

    //wifi_init_sta();    //Iniciamos configuracion y conexion de wifi station. Y despues mqtt

 	//rtc_time_start_timezone("CET-1CEST-2,M3.5.0/02:00:00,M10.5.0/03:00:00"); 	//Se especifica la zona horaria
    //initLedTricolor();   //Inicia configuracion LEDs

	servo_control_init();				//Inicia servo-motor

	//init_sensor_luminosidad();  		//Inicia sensor luminosidad
    //init_store_lux_data();				//Inicia proceso de almacenamiento de datos del sensor de luminosidad

	acelerometro_init();

	while(1){

		//ESP_LOGI(TAG,"pitch: %d, roll: %d, x: %f , y: %f , z: %f",pitch,roll,data.ax,data.ay,data.az);
		ESP_LOGI(TAG,"roll: %d",roll);

		dynamic_motor(roll);

        vTaskDelay(20/ portTICK_RATE_MS);
	}

}
