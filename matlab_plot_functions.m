
%Existen 2 casos para calcular todos los parámetros

%CASO 1) Deprecated

%El microcontrolador obtiene todos los datos y manualmente lo pegamos a la
%hoja de excel. Luego cargamos todos los datos con la funcion readtable.
%Tener en cuenta que hay que adaptar los datos en la hoja excel, explicado
%al final de este script.

%------------ INICIO DATOS OBTENIDO POR EL MICROCONTROLADOR ESP32 --------------------------------------------

filename = 'data.xlsx';

luxT = readtable('data.xlsx','Sheet','Hoja1','Range','B4:B42');
voltageT = readtable('data.xlsx','Sheet','Hoja1','Range','C4:C42');
lux_finalT = readtable('data.xlsx','Sheet','Hoja1','Range','D4:D42');
voltage_rawT = readtable('data.xlsx','Sheet','Hoja1','Range','E4:E42');

lux = transpose(table2array(luxT));
voltage = transpose(table2array(voltageT));
lux_final = transpose(table2array(lux_finalT));
voltage_raw = transpose(table2array(voltage_rawT));
lux_bulb = voltage * 22.5;


%------------- FIN DE DATOS OBTENIDO POR EL MICROCONTROLADOR ESP32----------------------------------------

%CASO 2)

%A partir de solamente los datos de luminosidad ambiental (lux) obtenidos
%por el microcontrolador ESP32, puedo obtener el resto de parametros con
%Matlab.

%------------ INICO DATOS OBTENIDO POR MATLAB --------------------------------------------


[numRows,numCols] = size(lux1);
for i = 1:numCols
    lux_avg(1,i) = (lux1(1,i)+lux2(1,i)+lux3(1,i)+lux4(1,i)+lux5(1,i)+lux6(1,i)+lux7(1,i))/7
end


[numRows,numCols] = size(lux);
target_lux = 140 * ones(numRows,numCols);   %Luminosidad objetivo (en lux)
ratio_lx_duty = 7647;      %Ratio entre lux obtenido y duty cycle aplicado para la bombilla
ratio_V_duty = 0784313;    %Ratio entre voltaje consumido y duty cycle aplicado para la bombilla
for i = 1:numCols   %Recorro todos los datos
    if lux(1,i) < target_lux(1,i) %Si la luz ambiental es menor que la objetivo
        differ(1,i) = target_lux(1,i) - lux(1,i); %Calcula la luz necesaria para llegar a la luz objetivo
        duty(1,i) = round(differ(1,i)/ratio_lx_duty); %Calcula el duty cycle necesario para aplicar al PWM y llegar a la luz objetivo
        voltage_n(1,i) = duty(1,i) * ratio_V_duty;    %Calcula el voltaje consumido por la bombilla con su respectivo PWM
        lux_final_n(1,i) = differ(1,i) + lux(1,i);     %Calcula la luz final tras sumar la ambiental y la generada por la bombilla
    else                        %Si la luz ambiental es mayor a la objetivo, no es necesario encender la bombilla
        differ(1,i) = 0;
        duty(1,i) = 0;
        voltage_n(1,i) = 0;
        lux_final_n(1,i) = lux(1,i);

    end
end

for i = 1:numCols
    if lux(1,i) < 70 
        voltage_raw_n(1,i) = 20;
    else 
        voltage_raw_n(1,i) = 0;
    end
end

%------------ FIN DATOS OBTENIDO POR MATLAB --------------------------------------------

%---------- TRATAMIENTO DE LOS DATOS OBTENIDOS (COMÚN A AMBOS CASOS) -------


filename = 'data.xlsx';
luxT1 = readtable('data.xlsx','Sheet','Hoja1','Range','G4:G291');
luxT2 = readtable('data.xlsx','Sheet','Hoja1','Range','H4:H291');
luxT3 = readtable('data.xlsx','Sheet','Hoja1','Range','I4:I291');
luxT4 = readtable('data.xlsx','Sheet','Hoja1','Range','J4:J291');
luxT5 = readtable('data.xlsx','Sheet','Hoja1','Range','K4:K291');
luxT6 = readtable('data.xlsx','Sheet','Hoja1','Range','L4:L291');
luxT7 = readtable('data.xlsx','Sheet','Hoja1','Range','M4:M291');

lux1 = transpose(table2array(luxT1));
lux2 = transpose(table2array(luxT2));
lux3 = transpose(table2array(luxT3));
lux4 = transpose(table2array(luxT4));
lux5 = transpose(table2array(luxT5));
lux6 = transpose(table2array(luxT6));
lux7 = transpose(table2array(luxT7));

[numRows,numCols] = size(lux1);
for i = 1:numCols
    lux_avg(1,i) = (lux1(1,i)+lux2(1,i)+lux3(1,i)+lux4(1,i)+lux5(1,i)+lux6(1,i)+lux7(1,i))/7
end

T=[0:0.0834:24];


%Luminosidad
plot(T,lux)
title('Tiempo Vs Lux')
hold on
plot(T,lux_bulb)
hold on
plot(T,lux_final)
hold off
legend('Ambiental','Bombilla','Corregida');


plot(T,lux1)
hold on
plot(T,lux2)
hold on
plot(T,lux3)
hold on
plot(T,lux4)
hold on
plot(T,lux5)
hold on
plot(T,lux6)
hold on
plot(T,lux7)
hold on
plot(T,lux_avg,'-k')
hold off
%xticks([0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24])
xticks([7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30])
title('Luminosity through a week','Fontsize',12,'FontWeight','bold')
xlabel('Time (Hours)','Position',[13 -25],'Fontsize',12,'FontWeight','bold');
ylabel('Luminosity (Lux)','Position',[5 50],'Fontsize',12,'FontWeight','bold');
xlim([7 30]);
%xlim([0 24]);
ylim([0 925]);
legend('Average');
legend('08/12/21','09/12/21','10/12/21','11/12/21','12/12/21','13/12/21','14/12/21','Average');


%---------- FIN TRATAMIENTO DE LOS DATOS OBTENIDOS  -------

% Antes de cargar el excel, se tiene que adecuar la pagina excel al formato
% necesario:
% 1) copiar todos los numeros del command prompt de ESP-IDF a el excel a
% mano
% 2) estaran todos en una casilla, asi que separarlos con Datos->Texto en
% columnas->Delimitados->Espacio->Siguiente->Finalizar. Con esto los separa
% por casilla cada vez que se encuentre un espacio.
% 3) copiar todas las celdas, pinchas en otra click derecho->pegado
% especial->trasponer. Ahora estan ordenadas verticalmente
% 4) sustituir cada . por , para que sea leible por matlab. Ctrl+B ->
% reemplazar . por ,
