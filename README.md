# Gateway Project

This project is based on the ESP32 microcontroller and some external peripherals. They are: light sensor,
LEDs, servo-motors and accelerometer/gyroscope. The project incorporates WiFi technology, and after connecting to the network, it  sends and receives data to and from other devices through MQTT communication with messages in JSON format.  

This ESP32 works as a gateway between the peripherals and an other ESP32, which will act as control interface as it has a LED screen and some buttons. Therefore, from now on, the current ESP32 will be called **Gateway** and the second ESP32, which is based on the "Interface" project will be called **Interface**.

The idea is that the user can directly interact with **Interface**, which sends the instructions to the **Gateway** and finally make the actuators do exactly what the user stated. On the other hand, the sensors offer data, collected by **Gateway** and sent back to **Interface**, which will show it on the screen.

<br>

## 1st Application: Luminosity 

One of the most interesting feature is that is capable to store data in the the flash memory of the ESP32. This means that no matter how many resets it does that the data will be always saved. Thanks to this, it stores the luminosity sensor data every 5 minutes during the 24 hours of the day and during the 7 days of the week, making a total of 2016 measurements. After this, all this data is printed in the console and passed it to an excel file, and next, the "matlab_plot_functions.m" file of Matlab plots the data in a graph.

The setup of the board is the following: an ESP32 DevKit and the VEML6030 luminosity sensor.

<br>
<br>

![Semantic description of image](set_up.png "Image Title")

<br>
<br>

Moreover, the user can go to the "Luminosity" option in the screen of **Interface** to get to know the current luminosity. This information is obtained after the **Interface** sends a command to **Gateway** via a MQTT message and receives back the current luminosity.

<br>

<img src=luminosity.gif alt="Descripción del GIF" autoplay>

<br>
<br>
<br>

The following graph represents the natural luminosity of my house throughout the week. Obviously, no lights have been switched on so that the experiment is not altered. As it can be seen, the sunlight goes from 8:00 to 18:00, as it is expected in december in Valencia (Spain). The highest luminosity period goes from 10:00 to 14:00. Besides, it shows that there are some random ups and downs during these hours that occur due to the instability of sun rays. This happens due to the fact that the light rays are never direct, as they collapse with clouds or any other thing in the nature, making multiples light refractions that ends with the sensor receiving the light that may have bounced on every wall or object nearby.

<br>

![Semantic description of image](figure_lux_7_days.png "Image Title")

<br>

A deep explanation of this application is represented in the following video, which can be also accessed with audio in the url: https://www.instagram.com/reel/CrtLHiTPBMs/?utm_source=ig_web_copy_link&igsh=MzRlODBiNWFlZA==

<br>

<img src=video.gif alt="Descripción del GIF" autoplay>

<br>
<br>
<br>



## 2nd Application: Servomotors Control

The user can manually set the desired angle of the motor by pressing the bottons of the PCB of the **Interface** project. Then, this board sends a command via MQTT to **Gateway**, which receives it and sends the command to the servomotors. Moreover, while the angles change, the **Gateway** reads the current angle of the motor and sends it via MQTT to the **Interface** so it can display the angle value in the screen.

<br>

<img src=spin_on_button.gif alt="Descripción del GIF" autoplay>

<br>
<br>
<br>


It is also implemented the automatic spin of the motors, which means they will constantly rotate in a loop until they are ordered to stop. 

<br>

<img src=spin_infinite.gif alt="Descripción del GIF" autoplay>

<br>
<br>
<br>

As the **Gateway** makes the servomotor move after receiving a command from a MQTT message, it actually doesn't need that only the **Interface** can order it. That means that in the MQTT explorer the user can send the command to the **Gateway**.

<br>

<img src=spin_on_explorer.gif alt="Descripción del GIF" autoplay>

<br>

## 3rd Application: Gyroscope/Accelerometer

The user can go to the "Gyroscope" option in the screen of **Interface** to get to know the current angle of the gyroscope. For that, the **Interface** sends a command to **Gateway** via MQTT and this board, reads it from the gyroscope and send this information back to **Interface**.

The objetive of this application is to create a steering wheel simulator in which the user tilts the board that has the gyroscope connected. 

<br>

<img src=gyroscope.gif alt="Descripción del GIF" autoplay>

<br>
<br>
<br>

After this introduction of the project, the main libraries are going to be briefly described.


## main.c
Start of the NVS partition in order to save memory in the flash.

Start of WiFi communication with the network specified in the menuconfig.

Start of MQTT communication with messages in JSON format.

Start of LEDs control

Start of servo-motors control

Start of luminosity sensor

Management of data saving in the memory flash

Start of accelerometer control

Shows the obtained angles of the gyroscope (pitch and roll). An incoming project will be related to this

Sends luminosity data through MQTT to **Interface**.
Sends to **Interface** the current angle of the servo-motors.
Receives the angles set by the user in **Interface** and send them to the servo_motor.


## sensorLuminosidad.h
It obtains the luminosity data through I2C communication with the VEML6030 sensor

## gestionLuminosidad.h
Manages the saving of the luminosity sensor so it can be accessed after resets of the CPU

## wifi_station.h
Manages WiFi communication with the network specified in the menuconfig.

## mqtt.h
Manages the connection to the MQTT server from Eclipse right after successful connection to the WiFi network

## parse_json.h
Parsing of incoming and outgoing MQTT messages in JSON format

## nvs.h
Management of the saving process in the memory flash

## servo_motor.h
Start and control of servo-motors. Sends PWM signals to set the motor in a specified angle
Manual mode: send angle to the servo
Automatic mode: the servo rotates in a loop from minimum to maximum angles and vice versa

## conf.h
Obtains online local timezone after connecting to WiFi network

## acelerometro.h
Obtains accelerometer/gyroscope data

